/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

package wgextender.features.regionprotect;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.RegisteredListener;
import wgextender.WGExtender;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class WGOverrideListener implements Listener {

	private final ArrayList<Tuple<HandlerList, RegisteredListener>> overridenEvents = new ArrayList<>();

	public void inject() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		for (Method method : getClass().getMethods()) {
			if (method.isAnnotationPresent(EventHandler.class)) {
				Class<?> eventClass = method.getParameterTypes()[0];
				HandlerList hl = (HandlerList) eventClass.getMethod("getHandlerList").invoke(null);
				for (RegisteredListener listener : new ArrayList<>(Arrays.asList(hl.getRegisteredListeners()))) {
					if (listener.getListener().getClass() == getClassToReplace()) {
						overridenEvents.add(new Tuple<>(hl, listener));
						hl.unregister(listener);
					}
				}
			}
		}
		Bukkit.getPluginManager().registerEvents(this, WGExtender.getInstance());
	}

	public void uninject() {
		HandlerList.unregisterAll(this);
		for (Tuple<HandlerList, RegisteredListener> tuple : overridenEvents) {
			tuple.getO1().register(tuple.getO2());
		}
		overridenEvents.clear();
	}

	private static class Tuple<T1, T2> {
		private final T1 o1;
		private final T2 o2;
		public Tuple(T1 t1, T2 t2) {
			this.o1 = t1;
			this.o2 = t2;
		}
		public T1 getO1() {
			return o1;
		}
		public T2 getO2() {
			return o2;
		}
	}

	protected abstract Class<? extends Listener> getClassToReplace();

}

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

package wgextender.features.regionprotect.regionbased;

import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.session.handler.GameModeFlag;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import wgextender.utils.WGRegionUtils;

public class GameModeChange implements Listener {
    private final String wgGameModeFlagClassName = GameModeFlag.class.getName();
    private int wgGameModeFlagStackIndex = -1;

    @EventHandler(priority = EventPriority.LOWEST)
    public void on(PlayerGameModeChangeEvent event) {
        Player player = event.getPlayer();
        GameMode requiredMode = WGRegionUtils.getEnumFlag(player.getLocation(), DefaultFlag.GAME_MODE);
        if (requiredMode == null) return;
        if (event.getNewGameMode() == requiredMode) return;
        if (player.hasPermission("wgextender.admin")) return;
        if (this.isWorldGuardCalled()) return;
        player.sendMessage(ChatColor.RED + "В данном регионе используется режим " + requiredMode.name());
        event.setCancelled(true);
    }

    private boolean isWorldGuardCalled() {
        if (this.wgGameModeFlagStackIndex >= 0) {
            StackTraceElement[] stack = Thread.currentThread().getStackTrace();
            if (stack.length <= this.wgGameModeFlagStackIndex) return false;
            return stack[this.wgGameModeFlagStackIndex].getClassName().equals(this.wgGameModeFlagClassName);
        }
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        for (int i = 0; i < stack.length; i++) {
            StackTraceElement element = stack[i];
            if (!element.getClassName().equals(this.wgGameModeFlagClassName)) continue;
            this.wgGameModeFlagStackIndex = i;
            return true;
        }
        return false;
    }
}

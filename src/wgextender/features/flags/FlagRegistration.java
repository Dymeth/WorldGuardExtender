/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

package wgextender.features.flags;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import com.sk89q.worldguard.protection.flags.registry.UnknownFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import wgextender.utils.ReflectionUtils;
import wgextender.utils.WGRegionUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;

public class FlagRegistration {
	public static void registerFlag(Flag<?> flag) throws IllegalAccessException {
		//manually insert flag into the registry
		FlagRegistry registry = WorldGuardPlugin.inst().getFlagRegistry();
		@SuppressWarnings("unchecked")
		ConcurrentMap<String, Flag<?>> flagMap = (ConcurrentMap<String, Flag<?>>) ReflectionUtils.getField(registry.getClass(), "flags").get(registry);
		Flag<?> prevFlag = flagMap.put(flag.getName().toLowerCase(), flag);
		//change flag instance in every loaded region if had old one
		if (prevFlag != null) {
			for (RegionManager rm : WGRegionUtils.getRegionContainer().getLoaded()) {
				for (ProtectedRegion region : rm.getRegions().values()) {
					Map<Flag<?>, Object> flags = region.getFlags();
					Object prevValue = flags.remove(prevFlag);
					if (prevValue != null) {
						//unknown flag will store marshaled value as value directly, so we can try to unmarshal it
						if (prevFlag instanceof UnknownFlag) {
							try {
								Object unmarshalled = flag.unmarshal(prevValue);
								if (unmarshalled != null) {
									flags.put(flag, unmarshalled);
								}
							} catch (Throwable ignored) {
							}
						}
						//before reload instance probably, try to marshal value first to see if it is compatible
						else {
							try {
								//noinspection unchecked
								((Flag<Object>) flag).marshal(prevValue);
								flags.put(flag, prevValue);
							} catch (Throwable ignored) {
							}
						}
						region.setDirty(true);
					}
				}
			}
		}
	}
}

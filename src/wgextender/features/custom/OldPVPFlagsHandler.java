/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

package wgextender.features.custom;

import com.google.common.base.Function;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import wgextender.WGExtender;
import wgextender.features.flags.OldPVPAttackSpeedFlag;
import wgextender.features.flags.OldPVPNoBowFlag;
import wgextender.features.flags.OldPVPNoShieldBlockFlag;
import wgextender.utils.ReflectionUtils;
import wgextender.utils.WGRegionUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

public class OldPVPFlagsHandler implements Listener {

	protected final HashMap<UUID, Double> oldValues = new HashMap<>();
	protected Field functionsField;

	public void start() {
		functionsField = ReflectionUtils.getField(EntityDamageEvent.class, "modifierFunctions");
		Bukkit.getPluginManager().registerEvents(this, WGExtender.getInstance());
		Bukkit.getScheduler().scheduleSyncRepeatingTask(WGExtender.getInstance(), () -> {
			for (Player player : Bukkit.getOnlinePlayers()) {
				if (WGRegionUtils.isFlagTrue(player.getLocation(), OldPVPAttackSpeedFlag.getInstance())) {
					if (!oldValues.containsKey(player.getUniqueId())) {
						AttributeInstance attribute = player.getAttribute(Attribute.GENERIC_ATTACK_SPEED);
						oldValues.put(player.getUniqueId(), attribute.getBaseValue());
						attribute.setBaseValue(16.0);
					}
				} else {
					reset(player);
				}
			}
		}, 0, 1);
	}

	public void stop() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			reset(player);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onQuit(PlayerQuitEvent event) {
		reset(event.getPlayer());
	}

	private void reset(Player player) {
		Double oldValue = oldValues.remove(player.getUniqueId());
		if (oldValue != null) {
			player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(oldValue);
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.LOWEST)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		Entity entity = event.getEntity();
		if (entity instanceof Player) {
			Player player = (Player) entity;
			if (player.isBlocking() && WGRegionUtils.isFlagTrue(entity.getLocation(), OldPVPNoShieldBlockFlag.getInstance())) {
				try {
					@SuppressWarnings("unchecked")
					Map<DamageModifier, ? extends Function<? super Double, Double>> func = (Map<DamageModifier, ? extends Function<? super Double, Double>>) functionsField.get(event);

					double damage = event.getDamage() + event.getDamage(DamageModifier.HARD_HAT);
					//reset blocking modifier
					event.setDamage(DamageModifier.BLOCKING, 0);
					//recalculate other modifiers

					for (DamageModifier modifier : new DamageModifier[]{
							DamageModifier.ARMOR,
							DamageModifier.RESISTANCE,
							DamageModifier.MAGIC,
							DamageModifier.ABSORPTION
					}) {
						@SuppressWarnings("ConstantConditions")
						double modifierValue = func.get(modifier).apply(damage);
						event.setDamage(modifier, modifierValue);
						damage += modifierValue;
					}

				} catch (IllegalArgumentException | IllegalAccessException e) {
					WGExtender.getInstance().getLogger().log(Level.SEVERE, "Unable to recalculate blocking damage", e);
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onInteract(PlayerInteractEvent event) {
		if ((event.getHand() == EquipmentSlot.OFF_HAND) && (event.getPlayer().getInventory().getItemInOffHand().getType() == Material.BOW)) {
			if (WGRegionUtils.isFlagTrue(event.getPlayer().getLocation(), OldPVPNoBowFlag.getInstance())) {
				event.setCancelled(true);
			}
		}
	}
}

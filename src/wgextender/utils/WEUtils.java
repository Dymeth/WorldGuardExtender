/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

package wgextender.utils;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.regions.RegionOperationException;
import com.sk89q.worldedit.world.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class WEUtils {

	public static WorldEditPlugin getWorldEditPlugin() {
		return JavaPlugin.getPlugin(WorldEditPlugin.class);
	}

    public static Region getSelection(Player player) throws IncompleteRegionException {
		LocalSession session = getWorldEditPlugin().getSession(player);
		World world = session.getSelectionWorld();
		if (world == null) throw new IncompleteRegionException();
		return session.getSelection(world);
	}

	public static void expandVert(Player player) throws IncompleteRegionException, RegionOperationException {
		LocalSession session = getWorldEditPlugin().getSession(player);
		com.sk89q.worldedit.entity.Player wePlayer = getWorldEditPlugin().wrapPlayer(player);
		Region region = getSelection(player);
		World weWorld = region.getWorld();
		if (weWorld == null) throw new IncompleteRegionException();
		region.expand(
				new Vector(0, weWorld.getMaxY() + 1, 0),
				new Vector(0, -(weWorld.getMaxY() + 1), 0)
		);
		session.getRegionSelector(weWorld).learnChanges();
		session.getRegionSelector(weWorld).explainRegionAdjust(wePlayer, session);
	}
}
